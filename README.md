# AppBehaviour
Easy way to show Rating, version dialog and ads on the basis of AI user behaviour in Android.
 
<b>Also Support AndroidX</b>
  
## Setup

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
    ext {
            firebase_config_version = '17.0.0'
    }
}
```
#### Dependency 
[![](https://jitpack.io/v/org.bitbucket.droidhelios/app-behaviour.svg)](https://jitpack.io/#org.bitbucket.droidhelios/app-behaviour)
```gradle
dependencies {
    implementation 'org.bitbucket.droidhelios:app-behaviour:x.y'
}
```   
 

# Rating

## Usage 

Add this to your MainActivity class

```java
    appFeature = AppRating.getInstance(this, BuildConfig.VERSION_CODE); 

```

Basic methods of AppFeature are :

```java
     private void setRating() {
           final RelativeLayout container = findViewById(R.id.rating_container);
           appsBehaviour.init(new RemoteCallback() {
               @Override
               public void onComplete(String status, RemoteModel remoteData) {
                     appsBehaviour.showRating(container);
                           ----or----
                     if(!appsBehaviour.isRatingSubmitted()) {
                        PopupDialog.newInstance("Rate Us", "If you enjoy this app, please take a moment to rate this app", new PopupDialog.DialogListener() {
                            @Override
                            public void onOkClick(DialogFragment dialog) {
                                SocialUtil.openAppInPlayStore(ContinuousCaptureActivity.this);
                                appsBehaviour.ratingSubmitted();
                            }
    
                            @Override
                            public void onCancelClick(DialogFragment dialog) {
    
                            }
                        }).show(getSupportFragmentManager(), "popupDialog");
                    }
               }
    
               @Override
               public void onError(String message) {
    
               }
           });
    }
    // or
    private void showRatingAfterCounting(Activity activity) {
          AppRating.DEFAULT_RATING_COUNT = 3;
          AppRating.DEFAULT_RATING_MIN_REDIRECT_TO_PLAY_STORE = 5;
          AppRating.showRatingAfterCounting(this, container);
    }
    private void setVersionUpdate(Activity activity) {
           AppRating.getInstance(activity,BuildConfig.VERSION_CODE)
                   .initVersionProcess();
    }
    private void setVersionUpdate(Activity activity) {
           appFeature.isShowAds()
    }
    
    private boolean isShowAds() {
          return appFeature.isShowAds()
    }

```


## Firebase Setup [Remote Config](https://console.firebase.google.com)
 
 
- **Parameter Key**

Note : Replace parameter key value "com_likeandupdate" with your own app package name 
```java
    com_likeandupdate_appsfeature

```

- **Default value**
```json
{
  "rating": {
    "ui": {
      "positive_button": "Rate Us",
      "negative_button": "Remind me later",
      "title": "Rating",
      "body": "If you enjoy this app, please take a moment to rate this app",
      "rating_min_redirect_to_play_store": 5
    },
    "logic": {
      "sessionFirstInstallTime": 48,
      "sessionRepeatTime": 72,
      "sessionDifferenceTime": 24,
      "ratingShowOnlyOnce": false,
      "negativeButtonPressedAndNotShowRating": false
    }
  },
  "app_update": {
    "ui": {
      "positive_button": "Download",
      "negative_button": "Cancel",
      "title": "Update",
      "body": "There is newer version of this application available, Click Download to redirect google play store."
    },
    "logic": {
      "minimum_version": 1,
      "latest_version_code": 1,
      "notification_type": "ONCE"
    }
  }
}
```

# Search Bar
## Usage 

Add this to your layout.xml file

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.coordinatorlayout.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"  
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:clickable="true"
    android:focusable="false" >

    <com.google.android.material.appbar.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="55dp"
        android:background="@android:color/transparent">

        <com.google.android.material.appbar.CollapsingToolbarLayout
            android:layout_width="match_parent"
            android:layout_height="55dp"
            app:layout_scrollFlags="scroll|enterAlways|exitUntilCollapsed">

            <LinearLayout
                android:id="@+id/container_bar"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="horizontal"
                app:layout_collapseMode="none"/>

        </com.google.android.material.appbar.CollapsingToolbarLayout>

    </com.google.android.material.appbar.AppBarLayout>

    <RelativeLayout
        app:layout_behavior="@string/appbar_scrolling_view_behavior" 
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
 
    </RelativeLayout>
 

</androidx.coordinatorlayout.widget.CoordinatorLayout>

```  

Add this to your MainActivity class

```java
public class Product extends Fragment implements BarListener {
    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState); 
        initUi(v);
        addSearchTag(savedInstanceState);
    }
    
    private void addSearchTag(Bundle savedInstanceState) {
        SearchBar.newInstance(Product.this)
                .setEnableShortOption(new String[]{"Name", "Stock", "Sale Price"})
                .setEnableOrderOption(true)
                .setSearchHint("Search by name, unit, hsn")
                .show(savedInstanceState, getFragmentManager(), R.id.container_bar);
    }
    
    @Override
    public void onSearchTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onSearchShortChanged(SortBar shortType, OrderBar orderBy) {
        if (cList==null){
            return;
        }
        try {
            if(shortType.equals(SortBar.TYPE1)) {
                Collections.sort(cList, new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel item, ProductModel item2) {
                        String s1 = item.getProductName();
                        String s2 = item2.getProductName();
                        if(orderBy == OrderBar.ASCENDING) {
                            return s1.compareToIgnoreCase(s2);
                        }else{
                            return s2.compareToIgnoreCase(s1);
                        }
                    }
                });
            } else if(shortType.equals(SortBar.TYPE2)) {
                Collections.sort(cList, new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel item, ProductModel item2) {
                        Double data = BarUtil.parseDouble(item.getQuantity());
                        Double data2 = BarUtil.parseDouble(item2.getQuantity());
                        if (orderBy == OrderBar.ASCENDING) {
                            return data.compareTo(data2);
                        } else {
                            return data2.compareTo(data);
                        }
                    }
                });
            }else if(shortType.equals(SortBar.TYPE3)) {
                Collections.sort(cList, new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel item, ProductModel item2) {
                        Date date = DateUtil.getDate(item.getInvoiceDate(), "dd-MMM-yy");
                        Date date2 = DateUtil.getDate(item2.getInvoiceDate(), "dd-MMM-yy");
                        if(orderBy == OrderBar.ASCENDING) {
                            return date.compareTo(date2);
                        }else{
                            return date2.compareTo(date);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }
}

```

Add this to your ListAdapter class

```java
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ReyclerViewHolder>{
  
    private Context context;
    private ArrayList<ProductModel> items;
    private List<ProductModel> originalData;
    private ItemFilter mFilter = new ItemFilter();

    public ProductAdapter(Activity context, ArrayList<ProductModel> items) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context; 
        this.items = items;
        this.originalData = items;
    }
    ...
    ...
    
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ProductModel> list = originalData;

            int count = list.size();
            final ArrayList<ProductModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (ProductModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getProductName();
                        filterableText+= model.getUnitName();
                        filterableText+= model.getCodeHSN();
                        if (filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<ProductModel>) results.values;
            notifyDataSetChanged();
        }

    }
}

```
