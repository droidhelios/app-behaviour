package com.appbehaviour.bar.interfaces;

import com.appbehaviour.bar.util.OrderBar;
import com.appbehaviour.bar.util.SortBar;

public interface BarListener {

    void onSearchTextChanged(CharSequence charSequence);

    void onSearchShortChanged(SortBar shortType, OrderBar orderBy);

}
