package com.appbehaviour.bar;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.appbehaviour.R;
import com.appbehaviour.bar.interfaces.BarListener;
import com.appbehaviour.bar.util.BarAnimation;
import com.appbehaviour.bar.util.BarUtil;
import com.appbehaviour.bar.util.SortBar;

/**
 * @author Created by Abhijit on 3/15/2018.
 */
public class SortBarView extends Fragment implements View.OnClickListener {


    private BarListener mListener;
    private String[] sortName;
    private SearchBar parentRef;


    public static SortBarView newInstance(BarListener listener, SearchBar parentRef, String[] sortName) {
        SortBarView fragment = new SortBarView();
        BarAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mListener = listener;
        fragment.parentRef = parentRef;
        fragment.sortName = sortName;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.ab_search_bar_short, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        Button btnAction = v.findViewById(R.id.btn_apply);
        Button btnType1 = v.findViewById(R.id.btn_short_1);
        Button btnType2 = v.findViewById(R.id.btn_short_2);
        Button btnType3 = v.findViewById(R.id.btn_short_3);
        btnAction.setOnClickListener(this);
        btnType1.setOnClickListener(this);
        btnType2.setOnClickListener(this);
        btnType3.setOnClickListener(this);

        if (sortName != null && sortName.length > 0) {
            if (sortName.length == 1) {
                btnType1.setText(sortName[0]);
                btnType2.setVisibility(View.GONE);
                btnType3.setVisibility(View.GONE);
            }else if (sortName.length == 2) {
                btnType1.setText(sortName[0]);
                btnType2.setText(sortName[1]);
                btnType3.setVisibility(View.GONE);
            }else  {
                btnType1.setText(sortName[0]);
                btnType2.setText(sortName[1]);
                btnType3.setText(sortName[2]);
            }
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btn_apply) {
            BarUtil.hideKeyboard(getActivity());
            closeWindow();
        } else if (id == R.id.btn_short_1) {
            parentRef.updateShortBy(SortBar.TYPE1);
            if (mListener != null) {
                mListener.onSearchShortChanged(SortBar.TYPE1, parentRef.mOrderType);
            }
            closeWindow();
        } else if (id == R.id.btn_short_2) {
            parentRef.updateShortBy(SortBar.TYPE2);
            if (mListener != null) {
                mListener.onSearchShortChanged(SortBar.TYPE2, parentRef.mOrderType);
            }
            closeWindow();
        } else if (id == R.id.btn_short_3) {
            parentRef.updateShortBy(SortBar.TYPE3);
            if (mListener != null) {
                mListener.onSearchShortChanged(SortBar.TYPE3, parentRef.mOrderType);
            }
            closeWindow();
        }
    }

    private void closeWindow() {
        if (getFragmentManager() != null) {
            getFragmentManager().popBackStack();
        }
    }
}
