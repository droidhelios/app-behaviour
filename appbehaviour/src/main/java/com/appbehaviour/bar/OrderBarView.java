package com.appbehaviour.bar;



import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;

import com.appbehaviour.R;
import com.appbehaviour.bar.interfaces.BarListener;
import com.appbehaviour.bar.util.BarAnimation;
import com.appbehaviour.bar.util.BarUtil;
import com.appbehaviour.bar.util.OrderBar;

/**
 * @author Created by Abhijit on 3/15/2018.
 */
public class OrderBarView extends Fragment {


    private BarListener mListener;
    private SearchBar parentRef;
    private OrderBar mType = OrderBar.ASCENDING;


    public static OrderBarView newInstance(BarListener listener, SearchBar parentRef, OrderBar orderType) {
        OrderBarView fragment = new OrderBarView();
        BarAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mListener = listener;
        fragment.parentRef = parentRef;
        fragment.mType = orderType;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.ab_search_bar_order, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        Button btnAction = v.findViewById(R.id.btn_apply);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BarUtil.hideKeyboard(getActivity());
                parentRef.mOrderType = mType;
                parentRef.isAppliedShort = true;
                mListener.onSearchShortChanged(parentRef.mShortType, mType);
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
            }
        });

        RadioGroup rgType = v.findViewById(R.id.rg_type);
        RadioButton rbAsc = v.findViewById(R.id.type_1);
        RadioButton rbDsc = v.findViewById(R.id.type_2);
        if(mType == OrderBar.ASCENDING){
            rbAsc.setChecked(true);
        }else {
            rbDsc.setChecked(true);
        }
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.type_1) {
                    mType = OrderBar.ASCENDING;
                } else if (checkedId == R.id.type_2) {
                    mType = OrderBar.DESCENDING;
                }
            }
        });

    }


}
