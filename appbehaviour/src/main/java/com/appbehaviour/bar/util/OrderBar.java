package com.appbehaviour.bar.util;

/**
 * Usage
 * //    private void addSearchTag(Bundle savedInstanceState) {
 * //        if (savedInstanceState == null) {
 * //            getFragmentManager().beginTransaction()
 * //                    .replace(R.id.container_bar, SearchBar.newInstance(ClassName.this),"searchBar")
 * //                    .commit();
 * //        }
 * //    }
 */
public enum OrderBar {
    ASCENDING, DESCENDING;
}
