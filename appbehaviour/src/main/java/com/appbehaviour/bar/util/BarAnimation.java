package com.appbehaviour.bar.util;

import android.os.Build;
import android.transition.Slide;

import androidx.fragment.app.Fragment;

public class BarAnimation {

    private static final long ANIM_DURATION_MEDIUM = 500;

    public static void setSlideAnimation(Fragment fragment, int gravity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slideTransition = new Slide(gravity);
            slideTransition.setDuration(ANIM_DURATION_MEDIUM);
            fragment.setEnterTransition(slideTransition);
            fragment.setAllowEnterTransitionOverlap(true);
            fragment.setAllowReturnTransitionOverlap(true);
        }
    }
}
