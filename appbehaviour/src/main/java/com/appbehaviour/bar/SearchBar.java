package com.appbehaviour.bar;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.appbehaviour.R;
import com.appbehaviour.bar.interfaces.BarListener;
import com.appbehaviour.bar.util.BarAnimation;
import com.appbehaviour.bar.util.BarUtil;
import com.appbehaviour.bar.util.DetailsTransition;
import com.appbehaviour.bar.util.OrderBar;
import com.appbehaviour.bar.util.SortBar;


/**
 * @author Created by Abhijit on 5/22/2017.
 */
public class SearchBar extends Fragment {


    public SortBar mShortType = SortBar.TYPE1;
    public OrderBar mOrderType = OrderBar.ASCENDING;
    private ImageView ivImage;
    private FrameLayout flSearch;
    private BarListener mOrderListener;
    private Button btnShort, btnOrder;
    private String[] sortName;
    private String searchHint;
    private boolean isOrderEnabled = true;
    public boolean isAppliedShort = false;


    public void show(Bundle savedInstanceState, FragmentManager fragmentManager, int container) {
        if (savedInstanceState == null && fragmentManager != null) {
            BarAnimation.setSlideAnimation(this, Gravity.TOP);
            fragmentManager.beginTransaction()
                    .replace(container, this, "searchBar")
                    .commit();
        }
    }

    public static SearchBar newInstance(BarListener listener) {
        SearchBar fragment = new SearchBar();
        fragment.mOrderListener = listener;
        return fragment;
    }


    public SearchBar setEnableShortOption(String[] sortName) {
        this.sortName = sortName;
        return this;
    }

    public SearchBar setSearchHint(String searchHint) {
        this.searchHint = searchHint;
        return this;
    }

    public SearchBar setEnableOrderOption(boolean isOrderEnabled) {
        this.isOrderEnabled = isOrderEnabled;
        return this;
    }

    public void updateShortBy(SortBar type) {
        isAppliedShort = true;
        mShortType = type;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isAppliedShort)
            btnShort.setText("Sort by: " + getShortName(mShortType));
    }

    private String getShortName(SortBar type) {
        switch (type) {
            case TYPE1:
                return sortName[0];
            case TYPE2:
                return sortName[1];
            case TYPE3:
            default:
                return sortName[2];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ab_search_bar, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        ivImage = v.findViewById(R.id.iv_search_bar);
        flSearch = v.findViewById(R.id.fl_search);
        btnShort = v.findViewById(R.id.btn_short);
        btnOrder = v.findViewById(R.id.btn_order);
        flSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchBarView fragment = SearchBarView.newInstance(mOrderListener, searchHint);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    showTargetFragmentLollipop(fragment, ivImage);
                    return;
                }
                if (getFragmentManager() != null) {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container_bar, fragment)
                            .addToBackStack("searchBarView")
                            .commit();
                }
            }
        });

        btnShort.setVisibility(sortName == null ? View.INVISIBLE : View.VISIBLE);
        btnOrder.setVisibility(isOrderEnabled ? View.VISIBLE : View.INVISIBLE);

        btnShort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BarUtil.replaceFragment(getFragmentManager(),SortBarView.newInstance(mOrderListener, SearchBar.this, sortName), R.id.container_bar, "sortBarVIew");

            }
        });
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BarUtil.replaceFragment(getFragmentManager(),OrderBarView.newInstance(mOrderListener, SearchBar.this, mOrderType), R.id.container_bar, "orderBarView");
            }
        });

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showTargetFragmentLollipop(SearchBarView fragment, View forImageView) {
        fragment.setSharedElementEnterTransition(new DetailsTransition());
        fragment.setEnterTransition(new Fade());
        fragment.setExitTransition(new Fade());
        fragment.setSharedElementReturnTransition(new DetailsTransition());

        String transitionName = ViewCompat.getTransitionName(forImageView);
        if (getFragmentManager() != null && transitionName != null) {
            getFragmentManager()
                    .beginTransaction()
                    .addSharedElement(forImageView, transitionName)
                    .replace(R.id.container_bar, fragment, "searchBarView")
                    .addToBackStack("searchBarView")
                    .commit();
        }
    }

}
