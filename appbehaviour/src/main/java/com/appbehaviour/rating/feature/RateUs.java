package com.appbehaviour.rating.feature;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appbehaviour.R;
import com.appbehaviour.rating.AppRating;
import com.appbehaviour.rating.utility.Const;
import com.appbehaviour.rating.utility.DateUtil;
import com.appbehaviour.rating.utility.SocialUtil;
import com.appbehaviour.rating.utility.UIModel;
import com.appbehaviour.rating.utility.UserPreference;
import com.appbehaviour.rating.utility.Utility;

/**
 * This class is used to display rating screen on given layout
 *
 * @author Abhijit Rao
 * @version 1.0
 * @since 2018.12.07
 */
public class RateUs {

    private static final long DEFAULT_FIRST_INSTALL_TIME = 48;//2days
    private static final long DEFAULT_REPEAT_TIME = 72;//3days
    private static final long DEFAULT_DIFFERENCE_TIME = 24;//1days
    private static final boolean DEFAULT_SINGLE_USE_ONLY = true;
    private final Activity activity;
    private RelativeLayout viewGroup;
    private final String packageName;
    private final UserPreference userPref;

    private long sessionFirstInstallTime; //sessionFirstInstallTime
    private long sessionRepeatTime; //sessionRepeatTime
    private long sessionDifferenceTimeInHours;
    private boolean ratingShowOnlyOnce;

    //base functionality variables
    private String sessionInstallTime;
    private String sessionLastTime;
    private String sessionCurrentTime;
    private UIModel uiData;
    private boolean negativeButtonPressedAndNotShowRating;

    private RateUs(Activity activity, RelativeLayout viewGroup) {
        this.activity = activity;
        this.packageName = activity.getApplicationContext().getPackageName();
        this.viewGroup = viewGroup;
        //default initialization
        this.sessionFirstInstallTime = DEFAULT_FIRST_INSTALL_TIME;
        this.ratingShowOnlyOnce = DEFAULT_SINGLE_USE_ONLY;
        this.sessionRepeatTime = DEFAULT_REPEAT_TIME;
        this.sessionDifferenceTimeInHours = DEFAULT_DIFFERENCE_TIME;
        this.negativeButtonPressedAndNotShowRating = false;
        userPref = new UserPreference(activity, packageName);
    }

    private static RateUs rateUs;

    public static RateUs getInstance() {
        return rateUs;
    }

    public static RateUs newInstance(Activity activity, RelativeLayout container) {
        rateUs = new RateUs(activity, container);
        return rateUs;
    }

    public RateUs setSessionFirstInstallTime(long sessionFirstInstallTime) {
        this.sessionFirstInstallTime = sessionFirstInstallTime;
        return this;
    }

    public RateUs setSessionRepeatTime(long sessionRepeatTime) {
        this.sessionRepeatTime = sessionRepeatTime;
        return this;
    }

    public RateUs setSessionDifferenceTime(long sessionDifferenceTimeInHours) {
        this.sessionDifferenceTimeInHours = sessionDifferenceTimeInHours;
        return this;
    }

    public RateUs setRatingShowOnlyOnce(boolean ratingShowOnlyOnce) {
        this.ratingShowOnlyOnce = ratingShowOnlyOnce;
        return this;
    }

    public RateUs setNegativeButtonPressedAndNotShowRating(boolean negativeButtonPressedAndNotShowRating) {
        this.negativeButtonPressedAndNotShowRating = negativeButtonPressedAndNotShowRating;
        return this;
    }

    public RateUs setUiData(UIModel uiModel) {
        this.uiData = uiModel;
        return this;
    }

    public void init() {
        sessionInstallTime = userPref.getInstallationDate();
        sessionLastTime = userPref.getLastUsageDate();
        sessionCurrentTime = userPref.getCurrentDate();

        startDateValidationProcess();
    }

    private boolean isShowUI = false;

    public void showUI(RelativeLayout relativeLayout) {
        viewGroup = relativeLayout;
        if (isShowUI) {
            addScreenOnView();
        }
    }


    public void showUIOnCount(RelativeLayout container) {
        viewGroup = container;
        if (isShowOnCountComplete()) {
            addScreenOnView();
        }
    }


    public boolean isShowUI() {
        return isShowUI;
    }

    private void startDateValidationProcess() {
        if (userPref.isFirstInstallCheck()) {
            if (isFirstInstallCheckSession()) {
                userPref.setFirstInstallCheck(false);
                isShowUI = true;
            }
        } else {
            if (isUserActiveAndValidSession()) {
                isShowUI = true;
            }
        }
    }

    private boolean isFirstInstallCheckSession() {
        long diffBwCurrentAndInstall = DateUtil.differenceBetween(sessionCurrentTime, sessionInstallTime);
        if (diffBwCurrentAndInstall > sessionFirstInstallTime) {
            if (diffBwCurrentAndInstall < sessionRepeatTime) {
                return true;
            } else {
                updateLastUsageDate(true);
            }
        }
        return false;
    }

    private boolean isUserActiveAndValidSession() {
        long diffBwCurrentAndLast = DateUtil.differenceBetween(sessionCurrentTime, sessionLastTime);
        if (diffBwCurrentAndLast > sessionDifferenceTimeInHours) {
            if (diffBwCurrentAndLast < sessionRepeatTime) {
                return true;
            } else {
                updateLastUsageDate(false);
            }
        }
        return false;
    }


    private void updateLastUsageDate(boolean isReInitializeProcess) {
        //update last usage date
        userPref.updateLastUsageDate();
        if (isReInitializeProcess) {
            //reinitialize installation date for restarting process again
            userPref.reInitializeInstallationDate();
        }
    }

    private void addScreenOnView() {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewGroup.removeAllViews();
        setViewHolder(inflater.inflate(R.layout.rating_screen, viewGroup));
    }

    private void setViewHolder(View view) {
        TextView tvMessage = view.findViewById(R.id.tv_message);
        Button negative = view.findViewById(R.id.btn_negative);
        Button positive = view.findViewById(R.id.btn_positive);
        final RatingBar ratingBar = view.findViewById(R.id.rating_bar);
        if (uiData != null) {
            String title = uiData.getTitle();
            if (!TextUtils.isEmpty(title)) {
                title = uiData.getTitle() + "\n";
            }
            if(!TextUtils.isEmpty(uiData.getBody())) {
                title += uiData.getBody();
            }
            if(!TextUtils.isEmpty(title)) {
                tvMessage.setText(title);
            }
            if(!TextUtils.isEmpty(uiData.getPositiveButton())) {
                positive.setText(uiData.getPositiveButton());
            }
            if(!TextUtils.isEmpty(uiData.getNegativeButton())) {
                negative.setText(uiData.getNegativeButton());
            }
        }
        if (ratingShowOnlyOnce) {
            userPref.setRatingShowOnlyOnce(true);
        }
        updateLastUsageDate(true);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewGroup.removeAllViews();
                userPref.setRatingCount(0);
                if (negativeButtonPressedAndNotShowRating) {
                    userPref.setRatingNegativeButtonPressedAndNotShow(true);
                }
            }
        });
        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingBar.getRating() > 0) {
                    int maxCount = uiData.getRatingMinRedirectToPlayStore();
                    if (uiData != null && ratingBar.getRating() >= maxCount) {
                        rateUs(activity);
                        Utility.showToast(activity, Const.RATING_TOAST_MESSAGE);
                        userPref.ratingSubmitted();
                    }else {
                        try {
                            SocialUtil.feedback(activity, AppRating.getInstance().getEmailId(), Const.FEEDBACK_MESSAGE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        userPref.ratingSubmitted();
                    }
                    viewGroup.removeAllViews();
                } else {
                    Utility.showToast(activity, Const.RATING_TOAST_ERROR_MESSAGE);
                }
            }
        });
    }

    private void rateUs(Activity activity) {
        Uri applicationUrl = Uri.parse(Utility.PLAY_STORE_URL + packageName);
        Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, applicationUrl);
        activity.startActivity(browserIntent1);
    }


    private boolean isShowOnCountComplete() {
        if(!userPref.isRatingSubmitted()) {
            int count = userPref.getRatingCount() + 1;
            if (count == AppRating.DEFAULT_RATING_COUNT) {
                return true;
            }else {
                userPref.setRatingCount(count);
                return false;
            }
        }else {
            return false;
        }
    }

}
