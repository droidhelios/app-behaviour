package com.appbehaviour.rating.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appbehaviour.R;
import com.appbehaviour.rating.utility.UIModel;
import com.appbehaviour.rating.utility.Utility;


public class VersionDialog {
    private Dialog dialog;
    private Boolean restrictToUpdate;
    private UIModel uiData;
    private Activity activity;

    public static VersionDialog newInstance(Activity activity, Boolean restrictToUpdate, UIModel uiData) {
        VersionDialog mInstance = new VersionDialog();
        mInstance.activity = activity;
        mInstance.restrictToUpdate = restrictToUpdate;
        mInstance.uiData = uiData;
        return mInstance;
    }
 
    public void show() {
        if ( activity != null && uiData != null ) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = LayoutInflater.from(activity);
            dialogBuilder.setView(viewHolder(inflater.inflate(R.layout.dialog_version, null)));
            dialogBuilder.setCancelable(!restrictToUpdate);
            dialog = dialogBuilder.create();
            dialog.show();
        }
    }

    private View viewHolder(View view) {
        TextView tvMessage = view.findViewById(R.id.tv_version_message);
        Button negative = view.findViewById(R.id.btn_negative);
        Button positive = view.findViewById(R.id.btn_positive);
        String title =  uiData.getTitle();
        if(!title.equals("")){
            title = uiData.getTitle() + "\n";
        }
        String message = title + uiData.getBody();
        tvMessage.setText(message);
        positive.setText(uiData.getPositiveButton());
        negative.setText(uiData.getNegativeButton());
        if(restrictToUpdate){
            negative.setVisibility(View.GONE);
        }
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update(activity);
                dialog.dismiss();
            }
        });
        return view;
    }

    private void update(Activity activity) {
        String packageName = activity.getApplicationContext().getPackageName();
        Uri applicationUrl = Uri.parse(Utility.PLAY_STORE_URL+ packageName);
        Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, applicationUrl);
        activity.startActivity(browserIntent1);
    }
}
