package com.appbehaviour.rating.interfaces;


public interface AdsCallback {

    void onComplete(Boolean status);
    void onError(Exception e);
}
