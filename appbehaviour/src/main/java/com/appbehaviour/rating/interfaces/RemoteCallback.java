package com.appbehaviour.rating.interfaces;


import com.appbehaviour.rating.utility.RemoteModel;

public interface RemoteCallback {

    void onComplete(String status, boolean isRatingSubmitted, RemoteModel remoteData);
    void onError(String message);
}
