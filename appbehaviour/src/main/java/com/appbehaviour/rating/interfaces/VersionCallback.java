package com.appbehaviour.rating.interfaces;

import com.appbehaviour.rating.utility.UIModel;

public interface VersionCallback {

    void showVersionDialog(Boolean restrictToUpdate, UIModel uiData);
}
