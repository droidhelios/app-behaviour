package com.appbehaviour.rating.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    /**
     * @param inputDate  = date
     * @param dateFormat like "dd-MMM-yy"
     * @return Date
     */
    public static Date getDate(String inputDate, String dateFormat) {
        Date date = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.US);
            date = format.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static long differenceBetween(String startDate, String endDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const.DEFAULT_DATE_FORMAT,Locale.US);
        long hours = 0;
        try {
            Date date1 = simpleDateFormat.parse(startDate);
            Date date2 = simpleDateFormat.parse(endDate);

            long diff = date1.getTime() - date2.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            hours = minutes / 60;
//            long days = hours / 24;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hours;
    }


    public static long convertDateToHour(String startDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const.DEFAULT_DATE_FORMAT,Locale.US);
        long hours = 0;
        try {
            Date date1 = simpleDateFormat.parse(startDate);

            long diff = date1.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            hours = minutes / 60;
//            long days = hours / 24;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hours;
    }


    public static String getDateStamp1() {
        return System.currentTimeMillis()+"";
    }
    public static String getDateStamp() {
        return new SimpleDateFormat(Const.DEFAULT_DATE_FORMAT, Locale.getDefault()).format(new Date());
    }

}
