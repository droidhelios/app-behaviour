package com.appbehaviour.rating.utility;

import com.appbehaviour.rating.AppRating;

public class UIModel {

    private String title;
    private String body;
    private String positiveButton;
    private String negativeButton;
    private int ratingMinRedirectToPlayStore = AppRating.DEFAULT_RATING_MIN_REDIRECT_TO_PLAY_STORE;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPositiveButton() {
        return positiveButton;
    }

    public void setPositiveButton(String positiveButton) {
        this.positiveButton = positiveButton;
    }

    public String getNegativeButton() {
        return negativeButton;
    }

    public void setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
    }

    public int getRatingMinRedirectToPlayStore() {
        return ratingMinRedirectToPlayStore == 0 ? AppRating.DEFAULT_RATING_MIN_REDIRECT_TO_PLAY_STORE : ratingMinRedirectToPlayStore;
    }

    public void setRatingMinRedirectToPlayStore(int ratingMinRedirectToPlayStore) {
        this.ratingMinRedirectToPlayStore = ratingMinRedirectToPlayStore;
    }
}
