package com.appbehaviour.rating.utility;

public class Rating {
    private long sessionFirstInstallTime;
    private long sessionRepeatTime;
    private long sessionDifferenceTime;
    private boolean ratingShowOnlyOnce;
    private boolean negativeButtonPressedAndNotShowRating;
    private UIModel uiModel;

    public UIModel getUiModel() {
        return uiModel;
    }

    public void setUiModel(UIModel uiModel) {
        this.uiModel = uiModel;
    }

    public long getSessionFirstInstallTime() {
        return sessionFirstInstallTime;
    }

    public void setSessionFirstInstallTime(long sessionFirstInstallTime) {
        this.sessionFirstInstallTime = sessionFirstInstallTime;
    }

    public long getSessionRepeatTime() {
        return sessionRepeatTime;
    }

    public void setSessionRepeatTime(long sessionRepeatTime) {
        this.sessionRepeatTime = sessionRepeatTime;
    }

    public long getSessionDifferenceTime() {
        return sessionDifferenceTime;
    }

    public void setSessionDifferenceTime(long sessionDifferenceTime) {
        this.sessionDifferenceTime = sessionDifferenceTime;
    }

    public boolean isRatingShowOnlyOnce() {
        return ratingShowOnlyOnce;
    }

    public void setRatingShowOnlyOnce(boolean ratingShowOnlyOnce) {
        this.ratingShowOnlyOnce = ratingShowOnlyOnce;
    }

    public boolean isNegativeButtonPressedAndNotShowRating() {
        return negativeButtonPressedAndNotShowRating;
    }

    public void setNegativeButtonPressedAndNotShowRating(boolean negativeButtonPressedAndNotShowRating) {
        this.negativeButtonPressedAndNotShowRating = negativeButtonPressedAndNotShowRating;
    }
}
