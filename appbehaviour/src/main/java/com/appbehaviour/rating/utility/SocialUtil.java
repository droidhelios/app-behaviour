package com.appbehaviour.rating.utility;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.appbehaviour.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SocialUtil {

    public static void feedback(Context context, String contactEmail) {
        feedback(context, contactEmail, null);
    }

    public static void feedback(Context context, String contactEmail, String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        List<Intent> targets = new ArrayList<Intent>();
        List<ResolveInfo> candidates = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo candidate : candidates) {
            String packageName = candidate.activityInfo.packageName;

            if (packageName.equals("com.google.android.gm") || packageName.equals("com.yahoo.mobile.client.android.mail") || packageName.equals("com.microsoft.office.outlook") || packageName.equals("com.google.android.apps.inbox")) {
                Intent iWantThis = new Intent(Intent.ACTION_SEND);
                iWantThis.setPackage(packageName);
                iWantThis.setType("text/plain");
                String[] recipient = {contactEmail};
                iWantThis.putExtra(Intent.EXTRA_EMAIL, recipient);
                iWantThis.putExtra(Intent.EXTRA_SUBJECT, "Application Feedback - " + context.getString(R.string.app_name));
                if (message != null) {
                    iWantThis.putExtra(Intent.EXTRA_TEXT, message);
                }
                iWantThis.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                targets.add(iWantThis);
            }

        }

        try {
            Intent chooser = Intent.createChooser(targets.remove(0), "Share With Email");
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, targets.toArray(new Parcelable[targets.size()]));
            context.startActivity(chooser);
        } catch (Exception e) {

        }
    }

    public static void shareExcelFile(Activity activity, File mOutputFile) {
        Uri uri = getUriFromFile(activity, mOutputFile);
        Intent in = new Intent(Intent.ACTION_SEND);
        in.setDataAndType(uri, "application/vnd.ms-excel");
        in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        in.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            activity.startActivity(Intent.createChooser(in, "Share With Fiends"));
        } catch (Exception e) {
            Toast.makeText(activity, "No application found to take action. Please download Google Drive/OneDrive/Dropbox from playstore.", Toast.LENGTH_LONG).show();
        }
    }

    public static Uri getUriFromFile(Activity activity, File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }

    public static void openAppInPlayStore(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public static void moreApps(Context context) {
        try {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW
                    , Uri.parse("https://play.google.com/store/apps/developer?id=" + context.getString(R.string.account_name_play_store)));
            context.startActivity(rateIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void share(Activity activity, String message) {
        try {
            Intent in = new Intent("android.intent.action.SEND");
            in.setType("text/plain");
            in.putExtra("android.intent.extra.TEXT", message);
            activity.startActivity(Intent.createChooser(in, "Share With Fiends"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openExcelFile(Activity activity, File mOutputFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = getUriFromFile(activity, mOutputFile);
        intent.setDataAndType(uri, "application/vnd.ms-excel");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
        }
    }

    public static void openPdfFile(Activity activity, File mOutputFile) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = getUriFromFile(activity, mOutputFile);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "No Pdf reader found please download PDF reader to view Invoice", Toast.LENGTH_LONG).show();
        }
    }

    public static void sharePdfFile(Activity activity, File mOutputFile) {
        Uri uri = getUriFromFile(activity, mOutputFile);
        Intent in = new Intent(Intent.ACTION_SEND);
        in.putExtra(Intent.EXTRA_STREAM, uri);
        in.setType("pdf/*");
        in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            activity.startActivity(Intent.createChooser(in, "Share With Fiends"));
        } catch (Exception e) {
            Toast.makeText(activity, "No application found to take action.", Toast.LENGTH_LONG).show();
        }
    }

}
