package com.appbehaviour.rating.utility;

public interface Const {
    String SUCCESS = "success";
    String FAILURE = "failure";
    String DEFAULT_DATE_FORMAT = "yyyyMMddHH";
    String CONTACT_EMAIL = "appsfeature@gmail.com";
    String FEEDBACK_MESSAGE = "Tell us what you don't like:";
    String RATING_TOAST_MESSAGE = "Please take a moment to rate this app on PlayStore, Give 5 star and motivate us for keep doing better.";
    String RATING_TOAST_ERROR_MESSAGE = "Please select your rating stars";
}
