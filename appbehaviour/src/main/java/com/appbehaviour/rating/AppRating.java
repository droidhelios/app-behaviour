package com.appbehaviour.rating;

import android.app.Activity;
import android.widget.RelativeLayout;

import com.appbehaviour.rating.feature.RateUs;
import com.appbehaviour.rating.feature.VersionUpdate;
import com.appbehaviour.rating.interfaces.RemoteCallback;
import com.appbehaviour.rating.interfaces.VersionCallback;
import com.appbehaviour.rating.ui.VersionDialog;
import com.appbehaviour.rating.utility.Const;
import com.appbehaviour.rating.utility.Rating;
import com.appbehaviour.rating.utility.RemoteConfig;
import com.appbehaviour.rating.utility.RemoteModel;
import com.appbehaviour.rating.utility.UIModel;
import com.appbehaviour.rating.utility.UserPreference;
import com.appbehaviour.rating.utility.Utility;
import com.appbehaviour.rating.utility.Version;

/**
 * @author Abhijit Rao
 * @version 1.0
 * @since 2018.12.07
 */
public class AppRating {

    private static AppRating _instance;
    private final Activity activity;
    private RelativeLayout container;
    private int appVersionCode;
    private String mRemoteConfigStatus;
    private RemoteModel mRemoteData;
    private UserPreference userPref;
    private boolean isRemoteSync = false;
    private String emailId;
    public static int DEFAULT_RATING_COUNT = 5;
    public static int DEFAULT_RATING_MIN_REDIRECT_TO_PLAY_STORE = 4;
    private boolean isDebugMode;

    public boolean isRemoteSync() {
        return isRemoteSync;
    }

    private AppRating(Activity activity, int appVersionCode) {
        this.activity = activity;
        this.userPref = new UserPreference(activity, activity.getApplicationContext().getPackageName());
        this.appVersionCode = appVersionCode;
    }

    public static AppRating getInstance(Activity activity, int appVersionCode) {
        if (_instance == null) {
            _instance = new AppRating(activity, appVersionCode);
        }
        return _instance;
    }

    public static AppRating getInstance() {
        return _instance;
    }

    public AppRating setRatingContainer(RelativeLayout container) {
        this.container = container;
        return this;
    }

    public AppRating init(final RemoteCallback remoteCallback) {
        RemoteConfig.newInstance(activity)
                .initialize()
                .fetch(new RemoteCallback() {
                    @Override
                    public void onComplete(String status, boolean isRatingSubmitted, RemoteModel remoteData) {
                        Utility.log(status);
                        isRemoteSync = true;
                        mRemoteConfigStatus = status;
                        mRemoteData = remoteData;
                        initRatingProcess();
                        if (remoteCallback != null) {
                            remoteCallback.onComplete(status, isRatingSubmitted(), remoteData);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        Utility.log(message);
                    }

                });
        return this;
    }

    public void initVersionProcess() {
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :102 (Invalid Request)");
            return;
        }
        Version version = mRemoteData.getVersion();
        VersionUpdate.newInstance(appVersionCode, version)
                .setNotificationType(version.getNotificationType())
                .setListener(new VersionCallback() {
                    @Override
                    public void showVersionDialog(Boolean restrictToUpdate, UIModel uiData) {
                        VersionDialog.newInstance(activity, restrictToUpdate, uiData)
                                .show();
                    }
                })
                .init();
    }

    public void initRatingProcess() {
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :103 (Invalid Request)");
            return;
        }
        Rating rating = mRemoteData.getRating();
        if (validateVisibility()) {
            RateUs.newInstance(activity, container)
                    .setSessionFirstInstallTime(rating.getSessionFirstInstallTime())
                    .setSessionRepeatTime(rating.getSessionRepeatTime())
                    .setSessionDifferenceTime(rating.getSessionDifferenceTime())
                    .setRatingShowOnlyOnce(rating.isRatingShowOnlyOnce())
                    .setNegativeButtonPressedAndNotShowRating(rating.isNegativeButtonPressedAndNotShowRating())
                    .setUiData(rating.getUiModel())
                    .init();
        }
    }

    public void showRating(RelativeLayout relativeLayout) {
        if (RateUs.getInstance() != null && relativeLayout != null) {
            RateUs.getInstance().showUI(relativeLayout);
        }
    }

    public boolean isShowAds() {
        if (RateUs.getInstance() != null) {
            return RateUs.getInstance().isShowUI();
        } else {
            init(null);
            return false;
        }
    }

    public void ratingSubmitted() {
        userPref.ratingSubmitted();
    }

    public boolean isRatingSubmitted() {
        return userPref.isRatingSubmitted();
    }


    private boolean validateVisibility() {
        boolean isOnce = userPref.isRatingShowOnlyOnce();
        boolean neverShow = userPref.isRatingNegativeButtonPressedAndNotShow();
        if (userPref.isRatingSubmitted() || isOnce || neverShow) {
            return false;
        }
        return true;
    }

    public String getEmailId() {
        return emailId == null ? Const.CONTACT_EMAIL : emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public static void showRatingAfterCounting(Activity activity, RelativeLayout container) {
        RateUs.newInstance(activity, container);
        RateUs.newInstance(activity, container).setUiData(new UIModel());
        if (RateUs.getInstance() != null && container != null) {
            RateUs.getInstance().showUIOnCount(container);
        }
    }

    public AppRating setDebugMode(boolean isDebugMode) {
        this.isDebugMode = isDebugMode;
        return this;
    }

    public boolean isDebugMode() {
        return isDebugMode;
    }
}
