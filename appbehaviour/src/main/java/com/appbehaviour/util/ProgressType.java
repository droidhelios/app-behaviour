package com.appbehaviour.util;

public enum ProgressType {
    SHOW_PROGRESS,
    SHOW_NO_DATA,
    HIDE_ALL
}
