package com.appbehaviour.util;

public interface BehaviourConstant {

    String NO_DATA = "No data";
    String NO_INTERNET_CONNECTION = "No internet connection.";
}
