package com.appbehaviour.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.TextView;

import com.appbehaviour.R;

public class AppBehaviourUtil {

    public static void showNoData(View view, int visibility) {
        showNoData(view, visibility, null);
    }


    public static void showNoData(View view, int visibility, String message) {
        if (view != null) {
            view.setVisibility(visibility);
            if (visibility == View.VISIBLE) {
                TextView tvNoData = view.findViewById(R.id.tv_no_data);
                if (view.findViewById(R.id.player_progressbar) != null) {
                    view.findViewById(R.id.player_progressbar).setVisibility(View.GONE);
                }
                if (tvNoData != null) {
                    tvNoData.setVisibility(View.VISIBLE);
                    if (!isConnected(view.getContext())) {
                        tvNoData.setText(BehaviourConstant.NO_INTERNET_CONNECTION);
                    } else {
                        tvNoData.setText(message == null ? BehaviourConstant.NO_DATA : message);
                    }
                }
            }
        }
    }

    public static void showLayoutProgress(View view, ProgressType type) {
        showLayoutProgress(view, type, null);
    }

    public static void showLayoutProgress(View view, ProgressType type, String message) {
        if (view != null) {
            if (type == ProgressType.HIDE_ALL) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
                TextView tvNoData = view.findViewById(R.id.tv_no_data);
                View pbProgress = view.findViewById(R.id.player_progressbar);
                if (tvNoData != null) {
                    tvNoData.setVisibility(type == ProgressType.SHOW_NO_DATA ? View.VISIBLE : View.GONE);
                }
                if (pbProgress != null) {
                    pbProgress.setVisibility(type == ProgressType.SHOW_PROGRESS ? View.VISIBLE : View.GONE);
                }
                if (type == ProgressType.SHOW_NO_DATA) {
                    if (tvNoData != null) {
                        tvNoData.setVisibility(View.VISIBLE);
                        if (!isConnected(view.getContext())) {
                            tvNoData.setText(BehaviourConstant.NO_INTERNET_CONNECTION);
                        } else {
                            tvNoData.setText(message == null ? BehaviourConstant.NO_DATA : message);
                        }
                    }
                }
            }

        }
    }

    public static boolean isConnected(Context context) {
        boolean isConnected = false;

        try {
            if (context != null && context.getSystemService(Context.CONNECTIVITY_SERVICE) != null && context.getSystemService(Context.CONNECTIVITY_SERVICE) instanceof ConnectivityManager) {
                ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                isConnected = false;
                if (connectivityManager != null) {
                    NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                    isConnected = activeNetwork != null && activeNetwork.isConnected();
                }
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return isConnected;
    }
}
