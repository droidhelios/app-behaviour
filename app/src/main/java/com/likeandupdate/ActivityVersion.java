package com.likeandupdate;

import android.app.Activity;
import android.os.Bundle;

import com.appbehaviour.rating.AppRating;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityVersion extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version);

        setVersionUpdate(this);
    }
    private void setVersionUpdate(Activity activity) {
        AppRating.getInstance(activity,BuildConfig.VERSION_CODE)
                .initVersionProcess();
    }
}
