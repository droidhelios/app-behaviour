package com.likeandupdate;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.appbehaviour.rating.AppRating;
import com.appbehaviour.rating.interfaces.RemoteCallback;
import com.appbehaviour.rating.utility.RemoteModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityRating extends AppCompatActivity {

    private AppRating appRating;
    private Boolean isShowOnRatingCount = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        appRating = AppRating.getInstance(this, BuildConfig.VERSION_CODE);
        setRating();
    }

    private void setRating() {
        final RelativeLayout container = findViewById(R.id.rating_container);
        if(!isShowOnRatingCount) {
            appRating.init(new RemoteCallback() {
                @Override
                public void onComplete(String status, boolean isRatingSubmitted, RemoteModel remoteData) {
                    if (!isRatingSubmitted) {
                        appRating.showRating(container);
                    }
                }

                @Override
                public void onError(String message) {

                }
            });
        }else {
            AppRating.DEFAULT_RATING_COUNT = 3;
            AppRating.DEFAULT_RATING_MIN_REDIRECT_TO_PLAY_STORE = 5;
            AppRating.showRatingAfterCounting(this, container);
        }
    }
}
