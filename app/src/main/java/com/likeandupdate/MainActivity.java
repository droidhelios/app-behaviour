package com.likeandupdate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.appbehaviour.rating.AppRating;
import com.appbehaviour.rating.interfaces.RemoteCallback;
import com.appbehaviour.rating.utility.RemoteModel;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppRating.getInstance(this, BuildConfig.VERSION_CODE)
                .setDebugMode(BuildConfig.DEBUG)
                .init(new RemoteCallback() {

                    @Override
                    public void onComplete(String status, boolean isRatingSubmitted, RemoteModel remoteData) {

                    }

                    @Override
                    public void onError(String message) {

                    }
                });
    }


    public void onRatingClick(View view) {
        startActivity(new Intent(MainActivity.this, ActivityRating.class));
    }

    public void onVersionClick(View view) {
        startActivity(new Intent(MainActivity.this, ActivityVersion.class));
    }
}
